### Component Configuration ###

Send data to and receive narrative from Narrative Science. Please contact your administrator to obtain token for API communication.   
#### Important parameters
 - Dimensions (specify 1 or 2 column names *present* in the data)
 - Story Type (barchart, linechart, piechart, scatterplot, histogram)
 - Verbosity (low=short narrative, medium, high=long narrative)

#### Final table format
 - timestamp
 - table_name
 - narrative
