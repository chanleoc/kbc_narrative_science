# Terms and Conditions

The Narrative Science component for KBC is offered by Narrative Science as a third party component. It is provided as-is, without guarantees and support, and for no additional charge. 
Component's task is to help user to send data and receive narrative from Narrative Science API to Keboola Connection Platform (KBC). 
API call is process by using user-entered token for API authentication, no sensitive information is being sent non-standard way, maintaining all Keboola recommended security standards along the way.

## FAQ
[Official website](https://narrativescience.com/Resources/Resource-Library/FAQs)

## Contact   
Narrative Science   
[Contact](https://narrativescience.com/Resources/About-Narrative-Science/Contact-Us)   